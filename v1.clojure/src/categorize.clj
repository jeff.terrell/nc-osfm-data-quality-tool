(ns categorize)

(defn dept->category
  [{:keys [dept_address fire_ind inactive_date stations status]}]
  (let [has-stations? (not-empty stations)
        has-station-1? (and has-stations?
                            (contains? stations 1))
        has-address? (some? dept_address)]
    (cond
      (not= "Y" fire_ind)                                :non-fire
      (and (some? inactive_date) (not= status "Active")) :inactive
      (and (nil?  inactive_date) (not= status "Active")) :semi-active1
      (and (some? inactive_date) (=    status "Active")) :semi-active2
      ;; Now we know that we have an active fire department.
      ;; Including comments about how to get dept. location in each case:
      ;; maybe check that S1 lat/lng agrees with geocoded addr lat/lng
      (and has-address? has-station-1?) :station-1-and-address
      ;; could use one of the S!1 lat/lng, and/or geocoded addr lat/lng
      (and has-address? has-stations?)  :address-no-station-1
      ;; must use geocoded addr lat/lng
      has-address?                      :address-no-stations
      ;; use S1 lat/lng
      has-station-1?                    :station-1-no-address
      ;; need some heuristic for reducing 1+ stations to a location
      has-stations?                     :no-station-1-no-address
      ;; no way to get dept location
      :else                             :no-stations-no-address)))
