(ns data
  (:require [babashka.fs :as fs]
            [clojure.edn :as edn]
            [clojure.java.io :as io]
            [clojure.string :as str])
  (:import java.io.PushbackReader))

(defn- data-file-name [date-str]
  (str (fs/path "data" (format "%s.edn" date-str))))

(defn dates-available []
  (let [fname->date #(first (str/split % #"\."))
        file->date (comp fname->date fs/file-name)]
    (->> "data" fs/list-dir (map file->date) distinct sort vec)))

(defn get-dataset [date-str]
  (with-open [in (->> date-str
                      data-file-name
                      io/reader
                      (new PushbackReader))]
    (edn/read in)))

(defn save-dataset! [date-str dataset]
  (spit (data-file-name date-str) (prn-str dataset)))

(defn update-date! [old-date new-date dataset]
  (let [dataset (assoc dataset :date-str new-date)]
    (save-dataset! new-date dataset)
    (fs/delete (data-file-name old-date))))
