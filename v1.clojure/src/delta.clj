(ns delta
  (:require [clojure.data :as data]
            [util :refer [invert-map]]))

(defn calc-delta [cat1 cat2]
  (let [[old new] (data/diff
                    (invert-map cat1)
                    (invert-map cat2))
        keys (distinct (mapcat keys [old new]))]
    (into (sorted-map)
          (for [key keys]
            [key {:old (get old key)
                  :new (get new key)}]))))
