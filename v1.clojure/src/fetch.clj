(ns fetch
  (:require [categorize :refer [dept->category]]
            [cheshire.core :as json]
            [clojure.java.io :as io]
            [data :as data]
            [util :refer [by-id mapset today]])
  (:import java.net.URI))

(defn- associate-stations [stations depts]
  (let [fdid->stations (group-by :fd_id stations)
        by-station-number (partial by-id :station_number)
        fdid->station-map (update-vals fdid->stations by-station-number)]
    (update-vals depts
                 (fn [{:keys [fd_id] :as dept}]
                   (let [stations (get fdid->station-map fd_id {})]
                     (assoc dept :stations stations))))))

(defn prep-data [{:keys [depts stations]}]
  "Given depts and stations data (parsed from JSON with keyword keys), return a
  map of:
  - :unknown-stations - vector of stations with an unknown fire department ID
  - :depts - map of dept ID -> dept info map, which includes a new :stations key
    for each, whose value is a map from station number to station data (itself a
    map)
  - :categories - map from category key to a set of dept IDs in that category."
  (let [known-dept? (mapset :fd_id depts)
        unknown-stations (remove (comp known-dept? :fd_id) stations)
        depts (associate-stations stations depts)
        categories (group-by dept->category depts)
        categories (update-vals categories #(mapset :fd_id %))]
    {:unknown-stations unknown-stations
     :depts (by-id :fd_id depts)
     :categories categories}))

(do ; hardcoded URLs
  (def ^:private depts-url (new URI "https://arcgis.ncdoi.com/server/rest/services/Hosted/Fire_Departments_Tbl/FeatureServer/0/query?where=1%3D1&objectIds=&time=&geometry=&geometryType=esriGeometryEnvelope&inSR=&spatialRel=esriSpatialRelIntersects&distance=&units=esriSRUnit_Foot&relationParam=&outFields=*&returnGeometry=true&maxAllowableOffset=&geometryPrecision=&outSR=&havingClause=&gdbVersion=&historicMoment=&returnDistinctValues=false&returnIdsOnly=false&returnCountOnly=false&returnExtentOnly=false&orderByFields=&groupByFieldsForStatistics=&outStatistics=&returnZ=false&returnM=false&multipatchOption=xyFootprint&resultOffset=&resultRecordCount=&returnTrueCurves=false&returnCentroid=false&sqlFormat=none&resultType=&datumTransformation=&f=pjson"))
  (def ^:private stations-url (new URI "https://arcgis.ncdoi.com/server/rest/services/Hosted/OSFM_Fire_Stations_FL/FeatureServer/0/query?where=1%3D1&objectIds=&time=&geometry=&geometryType=esriGeometryEnvelope&inSR=&spatialRel=esriSpatialRelIntersects&distance=&units=esriSRUnit_Foot&relationParam=&outFields=*&returnGeometry=true&maxAllowableOffset=&geometryPrecision=&outSR=&havingClause=&gdbVersion=&historicMoment=&returnDistinctValues=false&returnIdsOnly=false&returnCountOnly=false&returnExtentOnly=false&orderByFields=&groupByFieldsForStatistics=&outStatistics=&returnZ=false&returnM=false&multipatchOption=xyFootprint&resultOffset=&resultRecordCount=&returnTrueCurves=false&returnCentroid=false&sqlFormat=none&resultType=&datumTransformation=&f=pjson")))

(defn- fetch-json-url [url]
  (with-open [in (io/reader url)]
    (->> (json/parse-stream in keyword)
         :features
         (mapv :attributes))))

(defn fetch-data []
  (let [depts (fetch-json-url depts-url)
        stations (fetch-json-url stations-url)
        stations (mapv #(update % :station_number int) stations)
        dataset (prep-data {:depts depts, :stations stations})
        today (today)
        dataset (assoc dataset :date-str today)
        most-recent-date (last (data/dates-available))
        most-recent-ds (data/get-dataset most-recent-date)
        no-change? (->> [most-recent-ds dataset]
                        (map #(dissoc % :date-str))
                        (apply =))]
    (if no-change?
      (data/update-date! most-recent-date today dataset)
      (data/save-dataset! today dataset))
    dataset))
