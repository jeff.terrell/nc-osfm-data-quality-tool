(ns main
  (:require [org.httpkit.server :as srv]
            [router :refer [router]]))

(defn -main []
  (let [port 3001
        url (str "http://localhost:" port "/")]
    (srv/run-server router {:port port})
    (println "serving" url)
    @(promise)))
