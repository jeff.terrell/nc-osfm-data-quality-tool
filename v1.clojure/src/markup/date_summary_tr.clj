(ns markup.date-summary-tr
  (:require data
            [hiccup.core :as h]))

(defn markup [{:keys [unknown-stations categories date-str]}]
  (let [{:keys [non-fire inactive semi-active1 semi-active2
                station-1-and-address address-no-station-1
                address-no-stations station-1-no-address
                no-station-1-no-address
                no-stations-no-address]} categories]
    (h/html
      [:tr
       [:td [:a {:href (str "/details/" date-str)} date-str]]
       [:td (count unknown-stations)]
       [:td (count non-fire)]
       [:td (count inactive)]
       [:td (+ (count semi-active1) (count semi-active2))]
       [:td (count no-stations-no-address)]
       [:td (count no-station-1-no-address)]
       [:td (count address-no-stations)]
       [:td (count address-no-station-1)]
       [:td (count station-1-no-address)]
       [:td (count station-1-and-address)]])))
