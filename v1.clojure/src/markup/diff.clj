(ns markup.diff
  (:require [clojure.data :refer [diff]]
            [clojure.pprint :refer [pprint]]
            data
            [hiccup.core :as h]))

(defn- pp-text [dept]
  (with-out-str (pprint dept)))

(defn- diff-view [id old new]
  (let [[old new common] (diff old new)]
    (list [:h3 "Differences for dept #" id]
          [:div.side-by-side
           [:div
            [:h4 "Common to both"]
            [:pre [:code (pp-text common)]]]
           [:div
            [:h4 "Unique to old info"]
            [:pre [:code (pp-text old)]]]
           [:div
            [:h4 "Unique to new info"]
            [:pre [:code (pp-text new)]]]])))

(defn markup [date1 date2 id]
  (h/html
    (let [old (get-in (data/get-dataset date1) [:depts id])
          new (get-in (data/get-dataset date2) [:depts id])]
      (cond
        (nil? old) (list [:h3 "Only new info available:"]
                         [:pre [:code (pp-text new)]])
        (nil? new) (list [:h3 "Only old info available:"]
                         [:pre [:code (pp-text old)]])
        :else (diff-view id old new)))))
