(ns markup.fetch-button
  (:require [hiccup.core :as h]
            [util :refer [today]]))

(defn markup []
  (h/html
    [:p
     [:form {:method "post", :action "/dataset"}
      [:button {:disabled (= (today) (last (data/dates-available)))}
       "fetch and save new data"]]]))
