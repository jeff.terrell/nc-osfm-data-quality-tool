(ns page.compare
  (:require data
            [delta :refer [calc-delta]]
            [hiccup.core :as h]
            template
            [util :refer [by-id]]))

(defn- page [date1 date2]
  (let [ds1 (data/get-dataset date1)
        ds2 (data/get-dataset date2)
        delta (calc-delta (:categories ds1) (:categories ds2))
        old-depts (by-id :fd_id (:depts ds1))
        new-depts (by-id :fd_id (:depts ds2))
        id->dept (merge (:depts ds1) (:depts ds2))]
    (template/t
      (str "Summary of " (count delta) " changes between " date1 " and " date2)
      (h/html
        [:table
         [:thead
          [:tr [:th "FD ID"] [:th "County"] [:th "Department Name"] [:th "Old category"] [:th "New category"]]]
         [:tbody
          (for [[id {:keys [old new]}] delta
                :let [{:keys [county dept_name]} (get id->dept id)]]
            [:tr [:td [:a {:href "#"
                           :hx-get (format "/diff/%s/%s/%s" date1 date2 id)
                           :hx-target "#detail"
                           :hx-vals (format "{\"id\": \"%s\"}" id)}
                       id]]
             [:td county]
             [:td dept_name]
             [:td (or old "-")]
             [:td (or new "-")]])]]
        [:p#detail "(Click a department above to see details here.)"]))))
