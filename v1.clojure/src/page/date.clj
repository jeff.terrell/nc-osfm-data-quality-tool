(ns page.date
  (:require [clojure.string :as str]
            data
            [hiccup.core :as h]
            template))

(def ^:private to-string-fns
  {:unknown-stations (fn [{:keys [fd_id dept_name county station_number]}]
                       (format "%s County - %s (#%s, station #%s)" county dept_name fd_id station_number))
   :non-fire (fn [{:keys [fd_id dept_name county]}]
               (format "%s County - %s (#%s)" county dept_name fd_id))
   :inactive (fn [{:keys [fd_id dept_name county]}]
               (format "%s County - %s (#%s)" county dept_name fd_id))
   :semi-active1 (fn [{:keys [fd_id dept_name county status]}]
                   (format "%s County - %s (#%s) (status='%s')" county dept_name fd_id status))
   :semi-active2 (fn [{:keys [fd_id dept_name county inactive_date]}]
                   (format "%s County - %s (#%s) (inactive_date='%s')" county dept_name fd_id inactive_date))
   :no-stations-no-address (fn [{:keys [fd_id dept_name county]}]
                             (format "%s County - %s (#%s)" county dept_name fd_id))
   :no-station-1-no-address (fn [{:keys [fd_id dept_name county stations]}]
                              (format "%s County - %s (#%s) (station_numbers=%s)" county dept_name fd_id (str/join "," (-> stations keys sort))))
   :address-no-stations (fn [{:keys [fd_id dept_name county dept_address]}]
                          (format "%s County - %s (#%s) (address='%s')" county dept_name fd_id dept_address))
   :address-no-station-1 (fn [{:keys [fd_id dept_name county dept_address stations]}]
                           (format "%s County - %s (#%s) (address='%s') (station_numbers=%s)" county dept_name fd_id dept_address (str/join "," (-> stations keys sort))))
   :station-1-no-address (fn [{:keys [fd_id dept_name county]}]
                           (format "%s County - %s (#%s)" county dept_name fd_id))
   :station-1-and-address (fn [{:keys [fd_id dept_name county dept_address]}]
                            (format "%s County - %s (#%s) (address='%s')" county dept_name fd_id dept_address))})

;; There is/are <count> <desc>:
(def ^:private description
  {:unknown-stations "stations with an unknown FDID"
   :non-fire "non-fire departments"
   :inactive "inactive departments"
   :semi-active1 "departments that have a null inactive_date field yet have a status not equal to Active"
   :semi-active2 "departments that have a status of Active yet have a non-null inactive_date field"
   :no-stations-no-address "departments that have no stations and no address"
   :no-station-1-no-address "departments that have no address, and no station 1, but do have some stations"
   :address-no-stations "departments that have an address but no stations"
   :address-no-station-1 "departments that have an address and stations but no station 1"
   :station-1-no-address "departments that have a station 1 but no address"
   :station-1-and-address "departments with both a station 1 and an address"})

(defn- item-list [key items]
  (h/html
    [:details
     [:summary (str/join " " ["There" (if (= 1 (count items)) "is" "are")
                              (count items) (str (description key) ":")])]
     [:ol
      (letfn [(make-li [content] [:li content])]
        (map (comp make-li (to-string-fns key))
             (sort-by (juxt :county :fd_id) items)))]]))

(defn- dept-list-fn [{:keys [depts categories]}]
  (fn [key]
    (let [items (map depts (key categories))]
      (item-list key items))))

(defn- comparison-links [date-str]
  (h/html
    [:h3 "Compare to:"]
    [:ul
     (for [date (data/dates-available)
           :when (not= date date-str)]
       (let [cmp (compare date date-str)
             [d1 d2] (if (neg? cmp)
                       [date date-str]
                       [date-str date])
             url (format "/compare/%s/%s" d1 d2)]
         [:li [:a {:href url} date]]))]))

(defn page [date-str]
  (template/t
    (str "Details for " date-str)
    (let [{:keys [unknown-stations] :as ds} (data/get-dataset date-str)
          dept-list (dept-list-fn ds)
          {:keys [non-fire inactive semi-active1 semi-active2
                  no-stations-no-address no-station-1-no-address
                  address-no-stations address-no-station-1 station-1-no-address
                  station-1-and-address]} (:categories ds)]
      (list
        (comparison-links date-str)
        (item-list :unknown-stations unknown-stations)
        (map dept-list [:non-fire :inactive :semi-active1 :semi-active2
                        :no-stations-no-address :no-station-1-no-address
                        :address-no-stations :address-no-station-1
                        :station-1-no-address :station-1-and-address])))))
