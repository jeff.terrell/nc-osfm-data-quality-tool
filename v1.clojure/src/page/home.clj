(ns page.home
  (:require data
            [hiccup.core :as h]
            (markup
              [date-summary-tr :as date-summary-tr]
              [fetch-button :as fetch-button])
            template))

(defn- header-row []
  (h/html
    [:tr
     [:th "date"]
     [:th "unknown stations"]
     [:th "non-fire"]
     [:th "inactive"]
     [:th "semi-active"]
     [:th "no stations, no addr"]
     [:th "no station 1, no addr"]
     [:th "address, no stations"]
     [:th "address, no station 1"]
     [:th "station 1, no addr"]
     [:th "station 1 and addr"]]))

(defn page []
  (template/t
    "Home"
    (h/html
      (list
        [:style "td { text-align: center }"]
        (fetch-button/markup)
        [:table
         [:thead (header-row)]
         [:tbody
          (for [date (data/dates-available)
                :let [dataset (data/get-dataset date)]]
            (date-summary-tr/markup dataset))]]))))
