(ns router
  (:require [babashka.fs :as fs]
            [clojure.core.match :refer [match]]
            [clojure.string :as str]
            [fetch :refer [fetch-data]]
            (markup
              [date-summary-tr :as date-summary-tr]
              [diff :as diff]
              [fetch-button :as fetch-button])
            (page
              [compare :as compare]
              [date :as date]
              [home :as home])))

(def ^:private not-found {:status 404, :body "Error 404: Page not found"})

(defn- public-asset [subpath]
  (let [ext (second (re-matches #".*\.(.*)"
                                subpath))
        ctype (case ext
                ("js") "text/javascript"
                "application/octet-stream")
        path (fs/path "public" subpath)
        found? (and (fs/exists? path)
                    (fs/readable? path)
                    (fs/regular-file? path))]
    (if-not found?
      not-found
      {:body (-> path str slurp)
       :headers {"content-type" ctype}})))

(defn router [{:keys [query-string request-method uri] :as req}]
  (let [path (-> uri (str/split #"/") rest vec)]
    (match [request-method path]
           [:get []] {:body (home/page)}
           [:post ["dataset"]] (do (fetch-data)
                                   {:status  302
                                    :headers {"location" "/"}
                                    :body    ""})
           [:get ["details" date-str]] {:body (date/page date-str)}
           [:get ["compare" date1 date2]] {:body (compare/page date1 date2)}
           [:get ["diff" date1 date2 fd-id]] {:body (diff/markup date1 date2
                                                                 fd-id)}
           [:get ["public" subpath]] (public-asset subpath)
           :else not-found)))
