(ns template
  (:require [hiccup.core :as h]))

(def ^:private style
  (str ".side-by-side { display: flex; flex-direction: row; }\n"
       ".side-by-side > div { padding: 0 5px; }\n"))

(defn- t [title page-markup]
  (str
    "<!DOCTYPE html>\n"
    (h/html
      [:head
       [:meta {:charset "UTF-8"}]
       [:title (str title " | NC OSFM Department Data Quality Tool")]
       [:style style]
       [:script {:src "/public/htmx.min.js" :defer true}]]
      [:body
       [:header [:h1 title]]
       [:main page-markup]])))
