(ns util
  (:import java.time.ZonedDateTime
           java.time.format.DateTimeFormatter))

(defn invert-map [m]
  (into (empty m)
        (for [[k vs] m, v vs]
          [v k])))

(defn by-id [keyfn maps]
  (into {}
        (for [map maps]
          [(keyfn map) map])))

(def mapset (comp set map))

(defn today []
  (let [pattern (DateTimeFormatter/ofPattern "YYYY-MM-dd")
        now (ZonedDateTime/now)]
    (.format now pattern)))
