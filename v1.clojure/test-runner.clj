#!/usr/bin/env bb

(require '[clojure.test :as t]
         '[babashka.classpath :as cp])

(cp/add-classpath "src:test")

(def test-namespaces
  '[categorize-test
    delta-test
    fetch-test
    util-test])

(apply require test-namespaces)

(def test-results (apply t/run-tests test-namespaces))

(def failures-and-errors
  (let [{:keys [:fail :error]} test-results]
    (+ fail error)))

(System/exit failures-and-errors)
