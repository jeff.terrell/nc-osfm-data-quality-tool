(ns categorize-test
  (:require [categorize :as sut]
            [clojure.test :as t]))

(t/deftest dept->category-test
  (t/is (= :non-fire (sut/dept->category {:fire_ind nil})))
  (t/is (= :inactive (sut/dept->category {:fire_ind "Y", :inactive_date "something", :status "Inactive"})))
  (t/is (= :semi-active1 (sut/dept->category {:fire_ind "Y", :inactive_date nil, :status "Inactive"})))
  (t/is (= :semi-active2 (sut/dept->category {:fire_ind "Y", :inactive_date "something", :status "Active"})))
  (t/is (= :station-1-and-address (sut/dept->category {:fire_ind "Y", :inactive_date nil, :status "Active", :dept_address "an-addr", :stations {1 :station-1-info}})))
  (t/is (= :address-no-station-1 (sut/dept->category {:fire_ind "Y", :inactive_date nil, :status "Active", :dept_address "an-addr", :stations {2 :station-2-info}})))
  (t/is (= :address-no-stations (sut/dept->category {:fire_ind "Y", :inactive_date nil, :status "Active", :dept_address "an-addr", :stations nil})))
  (t/is (= :station-1-no-address (sut/dept->category {:fire_ind "Y", :inactive_date nil, :status "Active", :dept_address nil, :stations {1 :station-1-info}})))
  (t/is (= :no-station-1-no-address (sut/dept->category {:fire_ind "Y", :inactive_date nil, :status "Active", :dept_address nil, :stations {2 :station-2-info}})))
  (t/is (= :no-stations-no-address (sut/dept->category {:fire_ind "Y", :inactive_date nil, :status "Active", :dept_address nil, :stations nil}))))
