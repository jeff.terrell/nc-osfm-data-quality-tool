(ns delta-test
  (:require [delta :as sut]
            [clojure.test :as t]))

(t/deftest calc-delta-test
  (with-redefs [util/invert-map (constantly :unused-input-to-diff)
                clojure.data/diff (constantly [{1 :cat1a, 2 :cat2} {1 :cat1b, 3 :cat3}])]
    (t/is (= {1 {:old :cat1a, :new :cat1b}
              2 {:old :cat2, :new nil}
              3 {:old nil, :new :cat3}}
             (sut/calc-delta :old-stub :new-stub)))))
