(ns fetch-test
  (:require [fetch :as sut]
            [clojure.test :as t]))

(def ^:private known-fdid "01234")
(def ^:private unknown-station {:fd_id "UNKNOWN"})
(def ^:private station-number 7)
(def ^:private known-station {:fd_id known-fdid, :station_number station-number})
(def ^:private dept {:fd_id known-fdid, :dept_name "Foo dept"})

(t/deftest prep-data-test
  (t/is (= {:unknown-stations [unknown-station]
            :depts {known-fdid (assoc dept
                                      :stations {station-number known-station})}
            :categories {:non-fire #{known-fdid}}}
           (sut/prep-data {:depts [dept]
                           :stations [unknown-station known-station]}))))
