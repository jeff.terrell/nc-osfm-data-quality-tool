(ns util-test
  (:require [util :as sut]
            [clojure.test :as t]))

(t/deftest invert-map-test
  (t/is (= {1 :cat1, 2 :cat2, 3 :cat2}
           (sut/invert-map {:cat1 #{1}, :cat2 #{2 3}}))))

(t/deftest by-id-test
  (t/is (= {1 {:id 1}, 2 {:id 2}, 3 {:id 3}}
           (sut/by-id :id [{:id 1} {:id 2} {:id 3}]))))

(t/deftest mapset-test
  (t/is (= #{1 2 3}
         (sut/mapset :id [{:id 1} {:id 2} {:id 3} {:id 1}]))))
