use crate::department::{Department, RawDepartment};
use serde::{Deserialize, Serialize};

#[derive(Clone, Copy, Debug, Deserialize, PartialEq, Eq, Hash, Serialize)]
pub enum Category {
    NonFire,
    Inactive,
    SemiActive1,
    SemiActive2,
    Station1AndAddress,
    AddressNoStation1,
    AddressNoStations,
    Station1NoAddress,
    NoStation1NoAddress,
    NoStationsNoAddress,
}

pub fn all_categories() -> [Category; 10] {
    [
        Category::NonFire,
        Category::Inactive,
        Category::SemiActive1,
        Category::SemiActive2,
        Category::Station1AndAddress,
        Category::AddressNoStation1,
        Category::AddressNoStations,
        Category::Station1NoAddress,
        Category::NoStation1NoAddress,
        Category::NoStationsNoAddress,
    ]
}

impl Category {
    pub fn to_s(&self) -> &'static str {
        match self {
            Category::NonFire => "NonFire",
            Category::Inactive => "Inactive",
            Category::SemiActive1 => "SemiActive1",
            Category::SemiActive2 => "SemiActive2",
            Category::Station1AndAddress => "Station1AndAddress",
            Category::AddressNoStation1 => "AddressNoStation1",
            Category::AddressNoStations => "AddressNoStations",
            Category::Station1NoAddress => "Station1NoAddress",
            Category::NoStation1NoAddress => "NoStation1NoAddress",
            Category::NoStationsNoAddress => "NoStationsNoAddress",
        }
    }
}

pub fn categorize_department(dept: &Department) -> Category {
    let Department { stations, attrs } = dept;
    let RawDepartment { fire_ind, status, inactive_date, dept_address, .. } = attrs;
    if fire_ind.is_none() || fire_ind.as_ref().unwrap() != "Y" { return Category::NonFire }
    if let Some(_) = inactive_date {
        return if status == "Active" { Category::SemiActive2 } else { Category::Inactive }
    }
    if status != "Active" { return Category::SemiActive1 }
    let (has_stations, has_station_1) = match stations {
        Some(s) => (!s.is_empty(), s.contains_key("1")),
        None    => (false, false),
    };
    if dept_address.is_none() {
        return match (has_station_1, has_stations) {
            (true , _    ) => Category::Station1NoAddress,
            (false, true ) => Category::NoStation1NoAddress,
            (false, false) => Category::NoStationsNoAddress,
        };
    }
    return match (has_station_1, has_stations) {
        (true , _    ) => Category::Station1AndAddress,
        (false, true ) => Category::AddressNoStation1,
        (false, false) => Category::AddressNoStations,
    };
}

#[cfg(test)]
mod tests {
    use super::*;
    #[cfg(test)]
    use pretty_assertions::assert_eq;

    #[test]
    fn test_categorize_department() {
        let dept = Department {
            attrs: RawDepartment {
                fd_id: "01351".to_string(),
                county: "Cabarrus".to_string(),
                status: "Inactive".to_string(),
                inactive_date: Some(901843200000),
                ems_ind: None,
                fire_ind: None,
                rescue_ind: None,
                dept_id: 1734.0,
                dept_name: "CABARRUS COUNTY EMS".to_string(),
                dept_address: None,
                esri_oid: 10,
            },
            stations: None,
        };
        assert_eq!(categorize_department(&dept), Category::NonFire);
    }
}
