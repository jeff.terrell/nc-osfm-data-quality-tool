use crate::categorize::{categorize_department, Category};
use crate::department::Department;
use crate::station::Station;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

pub type DepartmentMap = HashMap<String, Department>;
pub type CategoriesMap = HashMap<Category, Vec<String>>;

#[derive(Debug, Deserialize, PartialEq, Serialize)]
pub struct Dataset {
    pub date_str: String,
    pub unknown_stations: Vec<Station>,
    pub departments: DepartmentMap,
    pub categories: CategoriesMap,
}

pub fn make_dataset(
    date_str: String,
    unknown_stations: Vec<Station>,
    departments: Vec<Department>,
) -> Dataset {
    let mut categories: CategoriesMap = HashMap::new();
    for dept in &departments {
        let category = categorize_department(&dept);
        let fdid = dept.attrs.fd_id.to_string();
        if let Some(fdids) = categories.get_mut(&category) {
            fdids.push(fdid);
        } else {
            categories.insert(category, Vec::from([fdid]));
        }
    }

    let mut dept_map: DepartmentMap = HashMap::new();
    departments.into_iter().for_each(|dept| {
        let key = dept.attrs.fd_id.to_string();
        dept_map.insert(key, dept);
    });

    Dataset {
        date_str,
        unknown_stations,
        departments: dept_map,
        categories,
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[cfg(test)]
    use crate::department::RawDepartment;
    use pretty_assertions::assert_eq;

    #[test]
    fn test_make_dataset() {
        let date_str = "2022-05-24".to_string();
        let department = Department {
            attrs: RawDepartment {
                fd_id: "01351".to_string(),
                county: "Cabarrus".to_string(),
                status: "Inactive".to_string(),
                inactive_date: Some(901843200000),
                ems_ind: None,
                fire_ind: None,
                rescue_ind: None,
                dept_id: 1734.0,
                dept_name: "CABARRUS COUNTY EMS".to_string(),
                dept_address: None,
                esri_oid: 10,
            },
            stations: None,
        };
        let mut departments = HashMap::new();
        departments.insert("01351".to_string(), department);
        let mut categories: CategoriesMap = HashMap::new();
        categories.insert(Category::NonFire, Vec::from(["01351".to_string()]));
        let department = Department {
            attrs: RawDepartment {
                fd_id: "01351".to_string(),
                county: "Cabarrus".to_string(),
                status: "Inactive".to_string(),
                inactive_date: Some(901843200000),
                ems_ind: None,
                fire_ind: None,
                rescue_ind: None,
                dept_id: 1734.0,
                dept_name: "CABARRUS COUNTY EMS".to_string(),
                dept_address: None,
                esri_oid: 10,
            },
            stations: None,
        };
        let expected = Dataset {
            date_str: date_str.to_string(),
            unknown_stations: Vec::new(),
            departments,
            categories,
        };
        let actual = make_dataset(date_str.to_string(), Vec::new(), Vec::from([department]));
        assert_eq!(actual, expected);
    }
}
