use crate::station::Station;
use curl::easy::Easy;
use serde::{Deserialize, Serialize};
use serde_json::{from_str, Value};
use std::collections::{HashMap, HashSet};
use std::str;

type StationMap = HashMap<String, Station>;

#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub struct RawDepartment {
    pub fd_id: String,
    pub county: String,
    pub status: String,
    pub inactive_date: Option<u64>,
    pub ems_ind: Option<String>,
    pub fire_ind: Option<String>,
    pub rescue_ind: Option<String>,
    pub dept_id: f64,
    pub dept_name: String,
    pub dept_address: Option<String>,
    pub esri_oid: u32,
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub struct Department {
    pub attrs: RawDepartment,
    pub stations: Option<StationMap>,
}

pub type Departments = Vec<Department>;

pub fn read_from_net(stations: Vec<Station>) -> Result<(Departments, Vec<Station>), String> {
    let url = "https://arcgis.ncdoi.com/server/rest/services/Hosted/Fire_Departments_Tbl/FeatureServer/0/query?where=1%3D1&objectIds=&time=&geometry=&geometryType=esriGeometryEnvelope&inSR=&spatialRel=esriSpatialRelIntersects&distance=&units=esriSRUnit_Foot&relationParam=&outFields=*&returnGeometry=true&maxAllowableOffset=&geometryPrecision=&outSR=&havingClause=&gdbVersion=&historicMoment=&returnDistinctValues=false&returnIdsOnly=false&returnCountOnly=false&returnExtentOnly=false&orderByFields=&groupByFieldsForStatistics=&outStatistics=&returnZ=false&returnM=false&multipatchOption=xyFootprint&resultOffset=&resultRecordCount=&returnTrueCurves=false&returnCentroid=false&sqlFormat=none&resultType=&datumTransformation=&f=pjson";

    let mut data = Vec::new();
    let mut handle = Easy::new();
    handle.url(url).unwrap();
    {
        let mut transfer = handle.transfer();
        transfer.write_function(|new_data| {
            data.extend_from_slice(new_data);
            Ok(new_data.len())
        }).unwrap();
        transfer.perform().unwrap();
    }
    let body = std::str::from_utf8(&data).unwrap();

    let parsed_json: Value = match from_str(&body) {
        Err(why) => return Err(format!("couldn't parse contents of departments response as JSON: {}", why)),
        Ok(value) => value,
    };
    let feats = &parsed_json["features"];
    let items = match &feats {
        Value::Array(items) => items,
        _ => return Err(format!(".features sub-object was not an array but was: {}", feats)),
    };
    let (raw_depts, errors): (Vec<_>, Vec<_>) = items.into_iter().map(|item| {
        let json = item["attributes"].to_string();
        return match from_str::<RawDepartment>(&json) {
            Err(why) => Err(format!("couldn't deserialize a RawDepartment from JSON '{}': {}", json, why)),
            Ok(department) => Ok(department),
        }}).partition(Result::is_ok);
    let raw_depts: Vec<_> = raw_depts.into_iter().map(Result::unwrap).collect();
    let errors: Vec<_> = errors.into_iter().map(Result::unwrap_err).collect();
    return if errors.len() > 0 {
        Err(format!("there were {} errors: {:?}", errors.len(), errors))
    } else {
        Ok(assoc_stations(raw_depts, stations))
    };
}

pub fn assoc_stations(
    raw_depts: Vec<RawDepartment>,
    stations: Vec<Station>,
) -> (Departments, Vec<Station>) {
    let known_fdids: HashSet<String> =
        HashSet::from_iter(raw_depts.iter().map(|d| d.fd_id.to_string()));
    let (stations, unknown_stations): (Vec<_>, Vec<_>) =
        stations.into_iter().partition(|s| known_fdids.contains(&s.fd_id));
    let mut smapmap: HashMap<String, StationMap> =
        stations.into_iter().fold(HashMap::new(), |mut smapmap, station| {
            let fd_id = station.fd_id.clone();
            let snumber = station.station_number.to_string();
            match smapmap.get_mut(&fd_id) {
                Some(sub) => { sub.insert(snumber, station); },
                None => { smapmap.insert(fd_id, HashMap::from([(snumber, station)])); },
            };
            smapmap
        });
    let depts: Departments = raw_depts.into_iter().map(|rd| {
        let fd_id = rd.fd_id.clone();
        Department {
            attrs: rd,
            stations: smapmap.remove(&fd_id),
        }
    }).collect();
    (depts, unknown_stations)
}

#[cfg(test)]
mod tests {
    use super::*;
    #[cfg(test)]
    use pretty_assertions::assert_eq;
    use serde_json::from_str;

    #[test]
    fn test_department_deserialize() {
        let test_json_str = "{ \
            \"fd_id\": \"00101\", \
            \"dept_name\": \"Altamahaw-Ossippee Fire Department, Inc.\", \
            \"dept_id\": 1, \
            \"dept_address\": \"2806 Old 87 Hwy  Elon 27244\", \
            \"status\": \"Active\", \
            \"county\": \"Alamance\", \
            \"fire_ind\": \"Y\", \
            \"rescue_ind\": \"Y\", \
            \"ems_ind\": \"Y\", \
            \"esri_oid\": 1, \
            \"inactive_date\": null \
        }";
        let result: RawDepartment = from_str(test_json_str).unwrap();
        let expected = RawDepartment {
            fd_id: "00101".to_string(),
            county: "Alamance".to_string(),
            status: "Active".to_string(),
            inactive_date: None,
            ems_ind: Some("Y".to_string()),
            fire_ind: Some("Y".to_string()),
            rescue_ind: Some("Y".to_string()),
            dept_id: 1.0,
            dept_name: "Altamahaw-Ossippee Fire Department, Inc.".to_string(),
            dept_address: Some("2806 Old 87 Hwy  Elon 27244".to_string()),
            esri_oid: 1,
        };
        assert_eq!(result, expected);
    }
}
