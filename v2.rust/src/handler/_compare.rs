use crate::categorize::Category;
use crate::dataset::{CategoriesMap, Dataset, DepartmentMap};
use crate::department::RawDepartment;
use crate::util::{available_dates, read_dataset};
use rocket::response::status::NotFound;
use rocket_dyn_templates::{context, Template};
use serde::Serialize;
use std::collections::HashMap;
type CatMap = HashMap<String, Category>;

#[get("/compare/<date1>/<date2>")]
pub fn compare(date1: String, date2: String) -> Result<Template, NotFound<String>> {
    let dates = available_dates();
    if !dates.contains(&date1) { return Err(NotFound(format!("date {} is not available", date1))) }
    if !dates.contains(&date2) { return Err(NotFound(format!("date {} is not available", date2))) }
    let Dataset { departments: depts1, categories: cats1, .. } = read_dataset(date1.to_string());
    let Dataset { departments: depts2, categories: cats2, .. } = read_dataset(date2.to_string());
    let id_to_cat1 = invert_indexes(cats1);
    let id_to_cat2 = invert_indexes(cats2);
    let deltas = deltas(&id_to_cat1, &id_to_cat2, &depts1, &depts2);
    Ok(Template::render("compare", context! { date1, date2, deltas }))
}

fn invert_indexes(categories: CategoriesMap) -> CatMap {
    let mut ret: CatMap = HashMap::new();
    for (cat, ids) in categories {
        for id in ids {
            ret.insert(id, cat);
        }
    }
    ret
}

fn deltas<'a>(
    id_to_cat1: &'a CatMap,
    id_to_cat2: &'a CatMap,
    depts1: &'a DepartmentMap,
    depts2: &'a DepartmentMap,
) -> Vec<Delta<'a>> {
    let mut deltas: Vec<Delta> = Vec::new();
    for (id, cat1) in id_to_cat1 {
        match id_to_cat2.get(&id.to_string()) {
            None => deltas.push(new_delta_l(&id, &depts1, &cat1)),
            Some(cat2) if *cat1 != *cat2 => deltas.push(new_delta_lr(&id, &depts2, &cat1, &cat2)),
            _ => (),
        }
    }
    for (id, cat2) in id_to_cat2 {
        match id_to_cat1.get(&id.to_string()) {
            None => deltas.push(new_delta_r(&id, &depts2, &cat2)),
            _ => (),
        }
    }
    deltas.sort_by(|a, b| a.id.cmp(&b.id));
    deltas
}

#[derive(Serialize)]
struct Delta<'a> {
    id: &'a str,
    county: &'a str,
    dept_name: &'a str,
    old_category: &'a str,
    new_category: &'a str,
}

fn new_delta_l<'a>(id: &'a str, depts: &'a DepartmentMap, old: &Category) -> Delta<'a> {
    new_delta(id, depts, old.to_s(), "-")
}

fn new_delta_r<'a>(id: &'a str, depts: &'a DepartmentMap, new: &Category) -> Delta<'a> {
    new_delta(id, depts, "-", new.to_s())
}

fn new_delta_lr<'a>(
    id: &'a str,
    depts: &'a DepartmentMap,
    new: &Category,
    old: &Category,
) -> Delta<'a> {
    new_delta(id, depts, old.to_s(), new.to_s())
}

fn new_delta<'a>(
    id: &'a str,
    depts: &'a DepartmentMap,
    old_category: &'a str,
    new_category: &'a str,
) -> Delta<'a> {
    let RawDepartment { county, dept_name, .. } = &depts[id].attrs;
    Delta { id, county, dept_name, old_category, new_category }
}
