use crate::categorize::all_categories;
use crate::dataset::{CategoriesMap, Dataset, DepartmentMap};
use crate::department::Departments;
use crate::util::{available_dates, read_dataset};
use rocket::response::status::NotFound;
use rocket_dyn_templates::{context, Template};
use std::cmp::Ordering;
use std::collections::HashMap;

#[get("/details/<date>")]
pub fn details(date: String) -> Result<Template, NotFound<String>> {
    let (found, prev_dates, post_dates) = partition_dates(&date);
    if !found {
        return Err(NotFound(format!("date {} is not available", date)));
    }
    let Dataset {
        unknown_stations,
        categories,
        departments,
        ..
    } = read_dataset(date.to_string());
    let data = deindex_categories(categories, departments);
    Ok(Template::render("details", context! { date, data, prev_dates, post_dates, unknown_stations }))
}

fn partition_dates(date: &str) -> (bool, Vec<String>, Vec<String>) {
    let mut prev_dates: Vec<String> = Vec::new();
    let mut post_dates: Vec<String> = Vec::new();
    let mut found = false;
    for d in available_dates() {
        match date.cmp(&d) {
            Ordering::Less => prev_dates.push(d),
            Ordering::Greater => post_dates.push(d),
            Ordering::Equal => found = true,
        }
    }
    (found, prev_dates, post_dates)
}

fn get_departments(depts: &mut DepartmentMap, ids: Vec<String>) -> Departments {
    let mut ret = Vec::with_capacity(ids.len());
    for id in ids {
        let dept = depts.remove(&id).unwrap();
        ret.push(dept);
    }
    sort_departments(ret)
}

fn sort_departments(mut depts: Departments) -> Departments {
    depts.sort_by(|a, b| match a.attrs.county.cmp(&b.attrs.county) {
        Ordering::Less => Ordering::Less,
        Ordering::Greater => Ordering::Greater,
        _ => a.attrs.fd_id.cmp(&b.attrs.fd_id),
    });
    depts
}

fn deindex_categories(
    mut cats: CategoriesMap,
    mut depts: DepartmentMap,
) -> HashMap<&'static str, Departments> {
    let all_cats = all_categories();
    let mut ret = HashMap::with_capacity(all_cats.len());
    for cat in all_cats {
        let ids = cats.remove(&cat).unwrap_or(Vec::new());
        ret.insert(cat.to_s(), get_departments(&mut depts, ids));
    }
    ret
}
