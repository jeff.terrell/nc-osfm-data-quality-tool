use crate::util::{available_dates, read_dataset};
use rocket::response::status::NotFound;
use rocket_dyn_templates::{context, Template};
use std::cmp::Ordering::Equal;
use std::collections::HashSet;

#[get("/diff/<date1>/<date2>/<id>")]
pub fn diff(date1: String, date2: String, id: String) -> Result<Template, NotFound<String>> {
    let dates = available_dates();
    if !dates.contains(&date1) { return Err(NotFound(format!("date {} is not available", date1))) }
    if !dates.contains(&date2) { return Err(NotFound(format!("date {} is not available", date2))) }
    let dataset1 = read_dataset(date1.to_string());
    let dataset2 = read_dataset(date2.to_string());
    if !dataset1.departments.contains_key(&id) && !dataset2.departments.contains_key(&id) {
        return Err(NotFound(format!("department #{} was found for neither {} nor {}", id, date1, date2)))
    }

    let dept1 = dataset1.departments.get(&id);
    let dept2 = dataset2.departments.get(&id);
    let mut comm: Vec<String> = Vec::new();
    let mut left: Vec<String> = Vec::new();
    let mut rite: Vec<String> = Vec::new();

    if dept1.is_none() {
        left.push("-".to_string());
        let dept2 = dept2.unwrap();
        rite.push(format!("county: {:?}", dept2.attrs.county));
        rite.push(format!("status: {:?}", dept2.attrs.status));
        rite.push(format!("inactive_date: {:?}", dept2.attrs.inactive_date));
        rite.push(format!("ems_ind: {:?}", dept2.attrs.ems_ind));
        rite.push(format!("fire_ind: {:?}", dept2.attrs.fire_ind));
        rite.push(format!("rescue_ind: {:?}", dept2.attrs.rescue_ind));
        rite.push(format!("dept_id: {:?}", dept2.attrs.dept_id));
        rite.push(format!("dept_name: {:?}", dept2.attrs.dept_name));
        rite.push(format!("dept_address: {:?}", dept2.attrs.dept_address));
        rite.push(format!("esri_oid: {:?}", dept2.attrs.esri_oid));

        if let Some(station_map) = &dept2.stations {
            for (snum, station) in station_map {
                rite.push(format!("station #{} - county: {:?}", snum, station.county));
                rite.push(format!("station #{} - latitude: {:?}", snum, station.latitude));
                rite.push(format!("station #{} - longitude: {:?}", snum, station.longitude));
                rite.push(format!("station #{} - station_address: {:?}", snum, station.station_address));
                rite.push(format!("station #{} - city: {:?}", snum, station.city));
                rite.push(format!("station #{} - state: {:?}", snum, station.state));
                rite.push(format!("station #{} - zip_code: {:?}", snum, station.zip_code));
                rite.push(format!("station #{} - dept_name: {:?}", snum, station.dept_name));
                rite.push(format!("station #{} - dept_status: {:?}", snum, station.dept_status));
                rite.push(format!("station #{} - dept_phone: {:?}", snum, station.dept_phone));
                rite.push(format!("station #{} - dept_inactive_date: {:?}", snum, station.dept_inactive_date));
                rite.push(format!("station #{} - removed_date: {:?}", snum, station.removed_date));
                rite.push(format!("station #{} - removed_ind: {:?}", snum, station.removed_ind));
                rite.push(format!("station #{} - chief_first_name: {:?}", snum, station.chief_first_name));
                rite.push(format!("station #{} - chief_middle_name: {:?}", snum, station.chief_middle_name));
                rite.push(format!("station #{} - chief_last_name: {:?}", snum, station.chief_last_name));
                rite.push(format!("station #{} - chief_email: {:?}", snum, station.chief_email));
                rite.push(format!("station #{} - objectid: {:?}", snum, station.objectid));
            }
        }

    } else if dept2.is_none() {
        rite.push("-".to_string());
        let dept1 = dept1.unwrap();
        left.push(format!("county: {:?}", dept1.attrs.county));
        left.push(format!("status: {:?}", dept1.attrs.status));
        left.push(format!("inactive_date: {:?}", dept1.attrs.inactive_date));
        left.push(format!("ems_ind: {:?}", dept1.attrs.ems_ind));
        left.push(format!("fire_ind: {:?}", dept1.attrs.fire_ind));
        left.push(format!("rescue_ind: {:?}", dept1.attrs.rescue_ind));
        left.push(format!("dept_id: {:?}", dept1.attrs.dept_id));
        left.push(format!("dept_name: {:?}", dept1.attrs.dept_name));
        left.push(format!("dept_address: {:?}", dept1.attrs.dept_address));
        left.push(format!("esri_oid: {:?}", dept1.attrs.esri_oid));

        if let Some(station_map) = &dept1.stations {
            for (snum, station) in station_map {
                left.push(format!("station #{} - county: {:?}", snum, station.county));
                left.push(format!("station #{} - latitude: {:?}", snum, station.latitude));
                left.push(format!("station #{} - longitude: {:?}", snum, station.longitude));
                left.push(format!("station #{} - station_address: {:?}", snum, station.station_address));
                left.push(format!("station #{} - city: {:?}", snum, station.city));
                left.push(format!("station #{} - state: {:?}", snum, station.state));
                left.push(format!("station #{} - zip_code: {:?}", snum, station.zip_code));
                left.push(format!("station #{} - dept_name: {:?}", snum, station.dept_name));
                left.push(format!("station #{} - dept_status: {:?}", snum, station.dept_status));
                left.push(format!("station #{} - dept_phone: {:?}", snum, station.dept_phone));
                left.push(format!("station #{} - dept_inactive_date: {:?}", snum, station.dept_inactive_date));
                left.push(format!("station #{} - removed_date: {:?}", snum, station.removed_date));
                left.push(format!("station #{} - removed_ind: {:?}", snum, station.removed_ind));
                left.push(format!("station #{} - chief_first_name: {:?}", snum, station.chief_first_name));
                left.push(format!("station #{} - chief_middle_name: {:?}", snum, station.chief_middle_name));
                left.push(format!("station #{} - chief_last_name: {:?}", snum, station.chief_last_name));
                left.push(format!("station #{} - chief_email: {:?}", snum, station.chief_email));
                left.push(format!("station #{} - objectid: {:?}", snum, station.objectid));
            }
        }

    } else {
        let dept1 = dept1.unwrap();
        let dept2 = dept2.unwrap();
        match dept1.attrs.county.cmp(&dept2.attrs.county) {
            Equal => comm.push(format!("county: {:?}", dept1.attrs.county)),
            _ => {
                left.push(format!("county: {:?}", dept1.attrs.county));
                rite.push(format!("county: {:?}", dept2.attrs.county));
            },
        }

        match dept1.attrs.status.cmp(&dept2.attrs.status) {
            Equal => comm.push(format!("status: {:?}", dept1.attrs.status)),
            _ => {
                left.push(format!("status: {:?}", dept1.attrs.status));
                rite.push(format!("status: {:?}", dept2.attrs.status));
            },
        }

        match dept1.attrs.inactive_date.cmp(&dept2.attrs.inactive_date) {
            Equal => comm.push(format!("inactive_date: {:?}", dept1.attrs.inactive_date)),
            _ => {
                left.push(format!("inactive_date: {:?}", dept1.attrs.inactive_date));
                rite.push(format!("inactive_date: {:?}", dept2.attrs.inactive_date));
            },
        }

        match dept1.attrs.ems_ind.cmp(&dept2.attrs.ems_ind) {
            Equal => comm.push(format!("ems_ind: {:?}", dept1.attrs.ems_ind)),
            _ => {
                left.push(format!("ems_ind: {:?}", dept1.attrs.ems_ind));
                rite.push(format!("ems_ind: {:?}", dept2.attrs.ems_ind));
            },
        }

        match dept1.attrs.fire_ind.cmp(&dept2.attrs.fire_ind) {
            Equal => comm.push(format!("fire_ind: {:?}", dept1.attrs.fire_ind)),
            _ => {
                left.push(format!("fire_ind: {:?}", dept1.attrs.fire_ind));
                rite.push(format!("fire_ind: {:?}", dept2.attrs.fire_ind));
            },
        }

        match dept1.attrs.rescue_ind.cmp(&dept2.attrs.rescue_ind) {
            Equal => comm.push(format!("rescue_ind: {:?}", dept1.attrs.rescue_ind)),
            _ => {
                left.push(format!("rescue_ind: {:?}", dept1.attrs.rescue_ind));
                rite.push(format!("rescue_ind: {:?}", dept2.attrs.rescue_ind));
            },
        }

        // can't compare f64's directly, so convert to strings first
        let dept_id1 = format!("{:?}", dept1.attrs.dept_id);
        let dept_id2 = format!("{:?}", dept2.attrs.dept_id);
        match dept_id1.cmp(&dept_id2) {
            Equal => comm.push(format!("dept_id: {:?}", dept1.attrs.dept_id)),
            _ => {
                left.push(format!("dept_id: {:?}", dept1.attrs.dept_id));
                rite.push(format!("dept_id: {:?}", dept2.attrs.dept_id));
            },
        }

        match dept1.attrs.dept_name.cmp(&dept2.attrs.dept_name) {
            Equal => comm.push(format!("dept_name: {:?}", dept1.attrs.dept_name)),
            _ => {
                left.push(format!("dept_name: {:?}", dept1.attrs.dept_name));
                rite.push(format!("dept_name: {:?}", dept2.attrs.dept_name));
            },
        }

        match dept1.attrs.dept_address.cmp(&dept2.attrs.dept_address) {
            Equal => comm.push(format!("dept_address: {:?}", dept1.attrs.dept_address)),
            _ => {
                left.push(format!("dept_address: {:?}", dept1.attrs.dept_address));
                rite.push(format!("dept_address: {:?}", dept2.attrs.dept_address));
            },
        }

        match dept1.attrs.esri_oid.cmp(&dept2.attrs.esri_oid) {
            Equal => comm.push(format!("esri_oid: {:?}", dept1.attrs.esri_oid)),
            _ => {
                left.push(format!("esri_oid: {:?}", dept1.attrs.esri_oid));
                rite.push(format!("esri_oid: {:?}", dept2.attrs.esri_oid));
            },
        }

        let snums1: HashSet<String> = match &dept1.stations {
            None => HashSet::new(),
            Some(stations) => {
                let mut set: HashSet<String> = HashSet::new();
                for key in stations.keys() {
                    set.insert(key.to_string());
                }
                set
            }
        };
        let snums2: HashSet<String> = match &dept2.stations {
            None => HashSet::new(),
            Some(stations) => {
                let mut set: HashSet<String> = HashSet::new();
                for key in stations.keys() {
                    set.insert(key.to_string());
                }
                set
            },
        };
        let scomm = snums1.intersection(&snums2);
        let sleft = snums1.difference(&snums2);
        let srite = snums2.difference(&snums1);

        for snum in scomm {
            let lstat = &dept1.stations.as_ref().unwrap()[snum];
            let rstat = &dept2.stations.as_ref().unwrap()[snum];

            match lstat.county.cmp(&rstat.county) {
                Equal => comm.push(format!("station #{} - county: {:?}", snum, lstat.county)),
                _ => {
                    left.push(format!("station #{} - county: {:?}", snum, lstat.county));
                    rite.push(format!("station #{} - county: {:?}", snum, rstat.county));
                },
            }

            // can't compare f64's directly, so convert to strings first
            let llat = format!("{:?}", lstat.latitude);
            let rlat = format!("{:?}", rstat.latitude);
            match llat.cmp(&rlat) {
                Equal => comm.push(format!("station #{} - latitude: {:?}", snum, llat)),
                _ => {
                    left.push(format!("station #{} - latitude: {:?}", snum, llat));
                    rite.push(format!("station #{} - latitude: {:?}", snum, rlat));
                },
            }

            // can't compare f64's directly, so convert to strings first
            let llong = format!("{:?}", lstat.longitude);
            let rlong = format!("{:?}", rstat.longitude);
            match llong.cmp(&rlong) {
                Equal => comm.push(format!("station #{} - longitude: {:?}", snum, llong)),
                _ => {
                    left.push(format!("station #{} - longitude: {:?}", snum, llong));
                    rite.push(format!("station #{} - longitude: {:?}", snum, rlong));
                },
            }

            match lstat.station_address.cmp(&rstat.station_address) {
                Equal => comm.push(format!("station #{} - station_address: {:?}", snum, lstat.station_address)),
                _ => {
                    left.push(format!("station #{} - station_address: {:?}", snum, lstat.station_address));
                    rite.push(format!("station #{} - station_address: {:?}", snum, rstat.station_address));
                },
            }

            match lstat.city.cmp(&rstat.city) {
                Equal => comm.push(format!("station #{} - city: {:?}", snum, lstat.city)),
                _ => {
                    left.push(format!("station #{} - city: {:?}", snum, lstat.city));
                    rite.push(format!("station #{} - city: {:?}", snum, rstat.city));
                },
            }

            match lstat.state.cmp(&rstat.state) {
                Equal => comm.push(format!("station #{} - state: {:?}", snum, lstat.state)),
                _ => {
                    left.push(format!("station #{} - state: {:?}", snum, lstat.state));
                    rite.push(format!("station #{} - state: {:?}", snum, rstat.state));
                },
            }

            match lstat.zip_code.cmp(&rstat.zip_code) {
                Equal => comm.push(format!("station #{} - zip_code: {:?}", snum, lstat.zip_code)),
                _ => {
                    left.push(format!("station #{} - zip_code: {:?}", snum, lstat.zip_code));
                    rite.push(format!("station #{} - zip_code: {:?}", snum, rstat.zip_code));
                },
            }

            match lstat.dept_name.cmp(&rstat.dept_name) {
                Equal => comm.push(format!("station #{} - dept_name: {:?}", snum, lstat.dept_name)),
                _ => {
                    left.push(format!("station #{} - dept_name: {:?}", snum, lstat.dept_name));
                    rite.push(format!("station #{} - dept_name: {:?}", snum, rstat.dept_name));
                },
            }

            match lstat.dept_status.cmp(&rstat.dept_status) {
                Equal => comm.push(format!("station #{} - dept_status: {:?}", snum, lstat.dept_status)),
                _ => {
                    left.push(format!("station #{} - dept_status: {:?}", snum, lstat.dept_status));
                    rite.push(format!("station #{} - dept_status: {:?}", snum, rstat.dept_status));
                },
            }

            match lstat.dept_phone.cmp(&rstat.dept_phone) {
                Equal => comm.push(format!("station #{} - dept_phone: {:?}", snum, lstat.dept_phone)),
                _ => {
                    left.push(format!("station #{} - dept_phone: {:?}", snum, lstat.dept_phone));
                    rite.push(format!("station #{} - dept_phone: {:?}", snum, rstat.dept_phone));
                },
            }

            match lstat.dept_inactive_date.cmp(&rstat.dept_inactive_date) {
                Equal => comm.push(format!("station #{} - dept_inactive_date: {:?}", snum, lstat.dept_inactive_date)),
                _ => {
                    left.push(format!("station #{} - dept_inactive_date: {:?}", snum, lstat.dept_inactive_date));
                    rite.push(format!("station #{} - dept_inactive_date: {:?}", snum, rstat.dept_inactive_date));
                },
            }

            match lstat.removed_date.cmp(&rstat.removed_date) {
                Equal => comm.push(format!("station #{} - removed_date: {:?}", snum, lstat.removed_date)),
                _ => {
                    left.push(format!("station #{} - removed_date: {:?}", snum, lstat.removed_date));
                    rite.push(format!("station #{} - removed_date: {:?}", snum, rstat.removed_date));
                },
            }

            match lstat.removed_ind.cmp(&rstat.removed_ind) {
                Equal => comm.push(format!("station #{} - removed_ind: {:?}", snum, lstat.removed_ind)),
                _ => {
                    left.push(format!("station #{} - removed_ind: {:?}", snum, lstat.removed_ind));
                    rite.push(format!("station #{} - removed_ind: {:?}", snum, rstat.removed_ind));
                },
            }

            match lstat.chief_first_name.cmp(&rstat.chief_first_name) {
                Equal => comm.push(format!("station #{} - chief_first_name: {:?}", snum, lstat.chief_first_name)),
                _ => {
                    left.push(format!("station #{} - chief_first_name: {:?}", snum, lstat.chief_first_name));
                    rite.push(format!("station #{} - chief_first_name: {:?}", snum, rstat.chief_first_name));
                },
            }

            match lstat.chief_middle_name.cmp(&rstat.chief_middle_name) {
                Equal => comm.push(format!("station #{} - chief_middle_name: {:?}", snum, lstat.chief_middle_name)),
                _ => {
                    left.push(format!("station #{} - chief_middle_name: {:?}", snum, lstat.chief_middle_name));
                    rite.push(format!("station #{} - chief_middle_name: {:?}", snum, rstat.chief_middle_name));
                },
            }

            match lstat.chief_last_name.cmp(&rstat.chief_last_name) {
                Equal => comm.push(format!("station #{} - chief_last_name: {:?}", snum, lstat.chief_last_name)),
                _ => {
                    left.push(format!("station #{} - chief_last_name: {:?}", snum, lstat.chief_last_name));
                    rite.push(format!("station #{} - chief_last_name: {:?}", snum, rstat.chief_last_name));
                },
            }

            match lstat.chief_email.cmp(&rstat.chief_email) {
                Equal => comm.push(format!("station #{} - chief_email: {:?}", snum, lstat.chief_email)),
                _ => {
                    left.push(format!("station #{} - chief_email: {:?}", snum, lstat.chief_email));
                    rite.push(format!("station #{} - chief_email: {:?}", snum, rstat.chief_email));
                },
            }

            match lstat.objectid.cmp(&rstat.objectid) {
                Equal => comm.push(format!("station #{} - objectid: {:?}", snum, lstat.objectid)),
                _ => {
                    left.push(format!("station #{} - objectid: {:?}", snum, lstat.objectid));
                    rite.push(format!("station #{} - objectid: {:?}", snum, rstat.objectid));
                },
            }
        }

        for snum in sleft {
            let station = &dept1.stations.as_ref().unwrap()[snum];
            left.push(format!("station #{} - county: {:?}", snum, station.county));
            left.push(format!("station #{} - latitude: {:?}", snum, station.latitude));
            left.push(format!("station #{} - longitude: {:?}", snum, station.longitude));
            left.push(format!("station #{} - station_address: {:?}", snum, station.station_address));
            left.push(format!("station #{} - city: {:?}", snum, station.city));
            left.push(format!("station #{} - state: {:?}", snum, station.state));
            left.push(format!("station #{} - zip_code: {:?}", snum, station.zip_code));
            left.push(format!("station #{} - dept_name: {:?}", snum, station.dept_name));
            left.push(format!("station #{} - dept_status: {:?}", snum, station.dept_status));
            left.push(format!("station #{} - dept_phone: {:?}", snum, station.dept_phone));
            left.push(format!("station #{} - dept_inactive_date: {:?}", snum, station.dept_inactive_date));
            left.push(format!("station #{} - removed_date: {:?}", snum, station.removed_date));
            left.push(format!("station #{} - removed_ind: {:?}", snum, station.removed_ind));
            left.push(format!("station #{} - chief_first_name: {:?}", snum, station.chief_first_name));
            left.push(format!("station #{} - chief_middle_name: {:?}", snum, station.chief_middle_name));
            left.push(format!("station #{} - chief_last_name: {:?}", snum, station.chief_last_name));
            left.push(format!("station #{} - chief_email: {:?}", snum, station.chief_email));
            left.push(format!("station #{} - objectid: {:?}", snum, station.objectid));
        }

        for snum in srite {
            let station = &dept2.stations.as_ref().unwrap()[snum];
            rite.push(format!("station #{} - county: {:?}", snum, station.county));
            rite.push(format!("station #{} - latitude: {:?}", snum, station.latitude));
            rite.push(format!("station #{} - longitude: {:?}", snum, station.longitude));
            rite.push(format!("station #{} - station_address: {:?}", snum, station.station_address));
            rite.push(format!("station #{} - city: {:?}", snum, station.city));
            rite.push(format!("station #{} - state: {:?}", snum, station.state));
            rite.push(format!("station #{} - zip_code: {:?}", snum, station.zip_code));
            rite.push(format!("station #{} - dept_name: {:?}", snum, station.dept_name));
            rite.push(format!("station #{} - dept_status: {:?}", snum, station.dept_status));
            rite.push(format!("station #{} - dept_phone: {:?}", snum, station.dept_phone));
            rite.push(format!("station #{} - dept_inactive_date: {:?}", snum, station.dept_inactive_date));
            rite.push(format!("station #{} - removed_date: {:?}", snum, station.removed_date));
            rite.push(format!("station #{} - removed_ind: {:?}", snum, station.removed_ind));
            rite.push(format!("station #{} - chief_first_name: {:?}", snum, station.chief_first_name));
            rite.push(format!("station #{} - chief_middle_name: {:?}", snum, station.chief_middle_name));
            rite.push(format!("station #{} - chief_last_name: {:?}", snum, station.chief_last_name));
            rite.push(format!("station #{} - chief_email: {:?}", snum, station.chief_email));
            rite.push(format!("station #{} - objectid: {:?}", snum, station.objectid));
        }
    }

    Ok(Template::render("diff", context! { date1, date2, id, dept1, dept2, comm, left, rite }))
}
