use crate::dataset::make_dataset;
use crate::department;
use crate::station;
use crate::util::available_dates;
use chrono;
use rocket::response::Redirect;
use ron;
use std::fs::File;
use std::io::prelude::*; // for File::write_all
use std::path::Path;

#[post("/fetch")]
pub fn fetch() -> Redirect {
    let response = Redirect::to("/");
    let dates = available_dates();
    let current_date = chrono::offset::Local::now().format("%Y-%m-%d").to_string();
    if !dates.is_empty() && current_date == dates[dates.len() - 1] {
        return response;
    }
    fetch_and_save_new_dataset(current_date);
    response
}

fn fetch_and_save_new_dataset(current_date: String) {
    let stations = station::read_from_net().expect(&format!("Error reading stations from net"));
    let (departments, unknown_stations) =
        department::read_from_net(stations).expect(&format!("Error reading departments from net"));
    let dataset = make_dataset(current_date.to_string(), unknown_stations, departments);
    let ron_str = match ron::to_string(&dataset) {
        Err(why) => panic!("RON serialization failed: {}", why),
        Ok(ron_str) => ron_str,
    };
    let path = Path::new("data").join(format!("{}.ron", current_date));
    let mut file = match File::create(&path) {
        Err(why) => panic!("couldn't create {}: {}", path.display(), why),
        Ok(file) => file,
    };
    match file.write_all(ron_str.as_bytes()) {
        Err(why) => panic!("couldn't write RON for {} to {}: {}", current_date, path.display(), why),
        Ok(_) => println!("successfully wrote {} bytes to {}", ron_str.len(), path.display()),
    }
}
