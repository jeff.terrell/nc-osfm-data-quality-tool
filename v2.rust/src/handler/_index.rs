use crate::categorize::all_categories;
use crate::dataset::Dataset;
use crate::util::{available_dates, read_dataset};
use rocket_dyn_templates::{context, Template};
use std::collections::HashMap;

#[get("/")]
pub fn index() -> Template {
    let dates = available_dates();
    let mut data: HashMap<String, HashMap<&'static str, usize>> = HashMap::new();
    for date in dates {
        let dataset = read_dataset(date.to_string());
        data.insert(date.to_string(), date_summary(dataset));
    }
    Template::render("index", context! { data })
}

fn date_summary(mut dataset: Dataset) -> HashMap<&'static str, usize> {
    let all_cats = all_categories();
    let mut ret = HashMap::with_capacity(all_cats.len());
    for cat in all_cats {
        let len = dataset.categories.remove(&cat).map(|x| x.len()).unwrap_or(0);
        ret.insert(cat.to_s(), len);
    }
    let sa1 = ret.remove("SemiActive1").unwrap_or(0);
    let sa2 = ret.remove("SemiActive2").unwrap_or(0);
    ret.insert("SemiActive", sa1 + sa2);
    ret.insert("UnknownStations", dataset.unknown_stations.len());
    ret
}
