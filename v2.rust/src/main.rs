mod categorize;
mod dataset;
mod department;
mod handler;
mod station;
mod util;

#[macro_use]
extern crate rocket;
use crate::handler as h;
use crate::util as u;
use rocket_dyn_templates::Template;

#[launch]
fn rocket() -> _ {
    let routes = routes![h::index, h::details, h::compare, h::diff, h::fetch];
    match u::make_data_dir() {
        Err(why) if why.kind() == std::io::ErrorKind::AlreadyExists => (),
        Err(why) => panic!("Unable to make data directory: {:?}", why),
        _ => (),
    }
    rocket::build()
        .mount("/", routes)
        .attach(Template::fairing())
}
