use curl::easy::Easy;
use serde::{Deserialize, Serialize};
use serde_json::{from_str, Value};
use std::str;

#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub struct Station {
    pub station_number     : f32,
    pub fd_id              : String,
    pub county             : String,
    pub latitude           : Option<f64>,
    pub longitude          : Option<f64>,
    pub station_address    : Option<String>,
    pub city               : Option<String>,
    pub state              : Option<String>,
    pub zip_code           : Option<String>,
    pub dept_name          : Option<String>,
    pub dept_status        : String,
    pub dept_phone         : Option<String>,
    pub dept_inactive_date : Option<u64>,
    pub removed_date       : Option<u64>,
    pub removed_ind        : Option<String>,
    pub chief_first_name   : Option<String>,
    pub chief_middle_name  : Option<String>,
    pub chief_last_name    : Option<String>,
    pub chief_email        : Option<String>,
    pub objectid           : u32,
}

pub fn read_from_net() -> Result<Vec<Station>, String> {
    let url = "https://arcgis.ncdoi.com/server/rest/services/Hosted/OSFM_Fire_Stations_FL/FeatureServer/0/query?where=1%3D1&objectIds=&time=&geometry=&geometryType=esriGeometryEnvelope&inSR=&spatialRel=esriSpatialRelIntersects&distance=&units=esriSRUnit_Foot&relationParam=&outFields=*&returnGeometry=true&maxAllowableOffset=&geometryPrecision=&outSR=&havingClause=&gdbVersion=&historicMoment=&returnDistinctValues=false&returnIdsOnly=false&returnCountOnly=false&returnExtentOnly=false&orderByFields=&groupByFieldsForStatistics=&outStatistics=&returnZ=false&returnM=false&multipatchOption=xyFootprint&resultOffset=&resultRecordCount=&returnTrueCurves=false&returnCentroid=false&sqlFormat=none&resultType=&datumTransformation=&f=pjson";

    let mut data = Vec::new();
    let mut handle = Easy::new();
    handle.url(url).unwrap();
    {
        let mut transfer = handle.transfer();
        transfer.write_function(|new_data| {
            data.extend_from_slice(new_data);
            Ok(new_data.len())
        }).unwrap();
        transfer.perform().unwrap();
    }
    let body = std::str::from_utf8(&data).unwrap();

    let parsed_json: Value = match from_str(&body) {
        Err(why) => return Err(format!("couldn't parse contents of station response as JSON: {}", why)),
        Ok(value) => value,
    };
    let feats = &parsed_json["features"];
    let items = match &feats {
        Value::Array(items) => items,
        _ => return Err(format!(".features sub-object was not an array but was: {}", feats)),
    };
    let (stations, errors): (Vec<_>, Vec<_>) = items.into_iter().map(|item| {
        let json = item["attributes"].to_string();
        return match from_str::<Station>(&json) {
            Err(why) => Err(format!("couldn't deserialize a Station from JSON '{}': {}", json, why)),
            Ok(station) => Ok(station),
        }}).partition(Result::is_ok);
    let stations: Vec<_> = stations.into_iter().map(Result::unwrap).collect();
    let errors: Vec<_> = errors.into_iter().map(Result::unwrap_err).collect();
    return if errors.len() > 0 {
        Err(format!("there were {} errors: {:?}", errors.len(), errors))
    } else {
        Ok(stations)
    };
}

#[cfg(test)]
mod tests {
    use super::*;
    #[cfg(test)]
    use pretty_assertions::assert_eq;
    use serde_json::from_str;

    #[test]
    fn test_station_deserialize() {
        let test_json_str = r#"{"chief_email":null,"chief_first_name":"Tina","chief_last_name":"Crouse","chief_middle_name":"T.","city":"Grimesland","county":"Pitt","dept_inactive_date":1232554168000,"dept_name":"Grimesland Fire Department","dept_phone":null,"dept_status":"Inactive","fd_id":"07417","latitude":null,"longitude":null,"objectid":7181,"removed_date":null,"removed_ind":null,"state":"NC","station_address":"4669 First Street in Grimesland","station_number":1.0,"zip_code":"27837"}"#;
        let result: Station = from_str(test_json_str).unwrap();
        let expected = Station {
            station_number: 1.0,
            fd_id: "07417".to_string(),
            county: "Pitt".to_string(),
            latitude: None,
            longitude: None,
            station_address: Some("4669 First Street in Grimesland".to_string()),
            city: Some("Grimesland".to_string()),
            state: Some("NC".to_string()),
            zip_code: Some("27837".to_string()),
            dept_name: Some("Grimesland Fire Department".to_string()),
            dept_status: "Inactive".to_string(),
            dept_phone: None,
            dept_inactive_date: Some(1232554168000),
            removed_date: None,
            removed_ind: None,
            chief_first_name: Some("Tina".to_string()),
            chief_middle_name: Some("T.".to_string()),
            chief_last_name: Some("Crouse".to_string()),
            chief_email: None,
            objectid: 7181,
        };
        assert_eq!(result, expected);
    }
}
