use crate::dataset::Dataset;
use ron::from_str;
use std::fs::{create_dir, DirEntry, read_dir, read_to_string};

pub fn make_data_dir() -> std::io::Result<()> {
    create_dir("data")?;
    Ok(())
}

pub fn available_dates() -> Vec<String> {
    let paths = read_dir("data").expect("Could not read data directory");
    let mut strs: Vec<String> = paths
        .into_iter()
        .map(Result::unwrap)
        .filter_map(valid_then_date_part)
        .collect();
    strs.sort();
    strs
}

pub fn read_dataset(date_str: String) -> Dataset {
    let fname = format!("data{}{}.ron", std::path::MAIN_SEPARATOR, date_str).to_string();
    let contents = read_to_string(&fname).expect("Something went wrong reading the file");
    let dataset = from_str(&contents).expect(&format!("Error reading dataset from file {}", fname));
    dataset
}

fn valid_then_date_part(path: DirEntry) -> Option<String> {
    let s: String = format!("{:?}", path.file_name());
    if valid_date(&s) {
        Some(s[1..11].to_string())
    } else {
        None
    }
}

// This isn't worth bringing the regex crate in for.
fn valid_date(str: &String) -> bool {
    let chars: Vec<char> = str.chars().collect();
    let is_digit = |ch| ch >= '0' && ch <= '9';
    chars.len() == 16
        && chars[0] == '"'
        && is_digit(chars[1])
        && is_digit(chars[2])
        && is_digit(chars[3])
        && is_digit(chars[4])
        && chars[5] == '-'
        && is_digit(chars[6])
        && is_digit(chars[7])
        && chars[8] == '-'
        && is_digit(chars[9])
        && is_digit(chars[10])
        && chars[11] == '.'
        && chars[12] == 'r'
        && chars[13] == 'o'
        && chars[14] == 'n'
        && chars[15] == '"'
}
